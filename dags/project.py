import pymongo
import requests
import datetime
import airflow
import json
from plugin.jsontomongo import insert_data_fromJson

from datetime import timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

#global variables

myclient = pymongo.MongoClient("mongodb://mongodb:27017/")
mydb = myclient["AppleFinancialPerformance"]
mycol = mydb["applestock"]

api_key_append="?apikey=jmpJcXbh7d3WCy9WfgWCYk1UULU2uZOu"
urlProfile = "https://financialmodelingprep.com/api/v3/profile/AAPL" + api_key_append
urlRating = "https://financialmodelingprep.com/api/v3/rating/AAPL" + api_key_append

data = {
    'rating':None,
    'profile':None,
    'current-timestamp': None
}


#functions
def fetch_apple_info():
    response_profile = requests.get(urlProfile)
    response_rating = requests.get(urlRating)
    if(response_profile.status_code == 200 and response_rating.status_code == 200):
        data['rating'] = response_rating.json
        data['profile'] = response_profile.json
        data['current-timestamp'] = datetime.datetime.now().isoformat()
    else:
        print("an error occured while retrieve the data")        

def fetch_to_json():
    fetch_apple_info()
    with open('/tmp/fetched_data.json', 'w') as json_file:
        json.dump(data, json_file, indent=4)

def insert_data():
    mycol.insert_one(data)

#dags
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'refresh_apple_data',
    default_args=default_args,
    schedule_interval=timedelta(days=1),
)

retrieve_task = PythonOperator(
    task_id="retrieve_data", 
    python_callable=fetch_apple_info,
    dag=dag,
)

insert_task = PythonOperator(
    task_id="insert_data", 
    python_callable=insert_data,
    dag=dag,
)

retrieve_to_json_task = PythonOperator(
    task_id="retrieve_data_to_json", 
    python_callable=fetch_to_json,
    dag=dag,
)

insert_from_json_task = PythonOperator(
    task_id="insert_data_from_json", 
    python_callable=insert_data_fromJson,
    dag=dag,
)

