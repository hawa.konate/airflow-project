from project import mycol, urlProfile, urlRating
import requests
import datetime
  
def fetch_rating():
    response_rating = requests.get(urlRating)
    if(response_rating.status_code == 200):
        with open('/tmp/response_rating.json', 'w') as json_file:
            json.dump(response_rating.json, json_file, indent=4)
            
def fetch_profile():
    response_profile = requests.get(urlProfile)
    if(response_profile.status_code == 200):
        with open('/tmp/response_profile.json', 'w') as json_file:
            json.dump(response_profile.json, json_file, indent=4)            
   

def insert_data_fromJson():
    resJson = open('/tmp/fetched_data.json')
    newData={}
    newData['rating'] = resJson['rating']
    newData['profile'] = resJson['rating']
    newData['current-timestamp'] = datetime.datetime.now()
    mycol.insert_one(newData)