import datetime as dt

from datetime import timedelta
from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator


args = {
    'id': '001',
    'owner': 'Hawa',
    'start_date': dt.datetime(2024, 3, 6),
    'schedule_interval': timedelta(days=1)
}


dag = DAG(
    'practice01',
    default_args=args,
    description='Practice test',
    schedule_interval=timedelta(days=1),
)

task1 = BashOperator(
    task_id='firstTask',
    bash_command='date',
    dag=dag,
)